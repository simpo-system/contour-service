package Aplication.Services;

import Aplication.Business.Mappers.ContourMapper;
import Aplication.Business.Process.ImageJProcessor;
import Models.ContourRequestData;
import Models.ContourResponse;
import Models.ContourResponseData;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ContourService {

    private ContourMapper contourMapper;
    private ImageJProcessor imageJProcessor;

    public ContourService(ContourMapper contourMapper, ImageJProcessor imageJProcessor) {
        this.contourMapper = contourMapper;
        this.imageJProcessor = imageJProcessor;
    }

    public ContourResponse contour(List<ContourRequestData> ctImages) {

        ctImages.stream().forEach(image -> this.imageJProcessor.Process(image.getImageName()));

        List<ContourResponseData> responseData = ctImages.stream().map(contourRequestData -> this.contourMapper.mapCTToContours(contourRequestData.getId(),contourRequestData.getImageName())).collect(Collectors.toList());

        ContourResponse response = new ContourResponse(responseData);

        return response;
    }
}
