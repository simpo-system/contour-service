package Aplication.Business.Mappers;

import Models.ContourResponse;
import Models.ContourResponseData;
import Models.ContourResponseImages;
import org.springframework.stereotype.Component;

@Component
public class ContourMapper {

    private final String segmentFolder = "Segment/";
    private final String contourFolder = "Contour/";
    private final String ROIFolder = "ROI/";
    private final String segmentSuffix = "_segment.jpg";
    private final String contourSuffix = "_contour.jpg";
    private final String ROISuffix = "_ROI.jpg";

    public ContourResponseData mapCTToContours(int id, String ctImageName) {

        ctImageName = "marcacion";

        String contourUrl =  contourFolder + ctImageName + contourSuffix;
        String ROIUrl =  ROIFolder + ctImageName + ROISuffix;
        String segmentUrl =  segmentFolder + ctImageName + segmentSuffix;

        ContourResponseImages data = new ContourResponseImages(contourUrl, ROIUrl, segmentUrl);

        return new ContourResponseData(id, data);

    }

}
