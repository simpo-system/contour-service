package Aplication.Business.Process;


import ij.IJ;
import ij.ImagePlus;
import ij.ImageStack;
import ij.gui.ImageRoi;
import ij.gui.Overlay;
import ij.process.ImageProcessor;
import inra.ijpb.binary.BinaryImages;
import inra.ijpb.data.image.ColorImages;
import inra.ijpb.data.image.Images3D;
import inra.ijpb.morphology.MinimaAndMaxima3D;
import inra.ijpb.morphology.Morphology;
import inra.ijpb.morphology.Strel;
import inra.ijpb.util.ColorMaps;
import inra.ijpb.watershed.Watershed;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.awt.*;
import java.awt.image.ColorModel;


@Component
public class ImageJProcessor {

    private Thread segmentationThread = null;
    @Value("${frontend.location}")
    private String contourBaseFolder;
    @Value("${ct.folder.location}")
    private String ctFolderLocation;

    public void Process(final String ctImageName) {

            final int readConn = 4;

            final int connectivity = 6;

            // read dynamic
            final double dynamic = 200.0;

            double max = Float.MAX_VALUE;

            if( dynamic < 0 || dynamic > max )
            {
                IJ.error( "Morphological Segmentation", "ERROR: the dynamic value must be a number between 0 and " + max );
                return;
            }

            final Thread oldThread = null;

            // Thread to run the segmentation
            Thread newThread = new Thread() {

                public void run()
                {

                    ImagePlus image = IJ.openImage(ctFolderLocation + ctImageName +".dcm");

                    // read dams flag
                    boolean calculateDams = true;

                    // get original image info
                    ImageStack imageStack = image.getImageStack();
                     int gradientRadius = 2;

                        IJ.log( "Applying morphological gradient to input image..." );

                        Strel strel = Strel.Shape.SQUARE.fromRadius( gradientRadius );
                        ImageProcessor gradient = null;
                        gradient = Morphology.gradient( imageStack.getProcessor( 1 ), strel );
                        imageStack = new ImageStack(imageStack.getWidth(), imageStack.getHeight());
                        imageStack.addSlice(gradient);


                        // store gradient image
                        ImageStack gradientStack = imageStack;

                    IJ.log( "Running extended minima with dynamic value " + dynamic + "..." );

                    // Run extended minima
                    ImageStack regionalMinima = MinimaAndMaxima3D.extendedMinima( imageStack, dynamic, connectivity );

                    if( null == regionalMinima )
                    {
                        IJ.log( "The segmentation was interrupted!" );
                        IJ.showStatus( "The segmentation was interrupted!" );
                        IJ.showProgress( 1.0 );
                        return;
                    }


                    IJ.log( "Imposing regional minima on original image (connectivity = " + readConn + ")..." );

                    // Impose regional minima over the original image
                    ImageStack imposedMinima = MinimaAndMaxima3D.imposeMinima( imageStack, regionalMinima, connectivity );

                    if( null == imposedMinima )
                    {
                        IJ.log( "The segmentation was interrupted!" );
                        IJ.showStatus( "The segmentation was interrupted!" );
                        IJ.showProgress( 1.0 );
                        return;
                    }


                    // Label regional minima
                    ImageStack labeledMinima = BinaryImages.componentsLabeling( regionalMinima, connectivity, 32 );
                    if( null == labeledMinima )
                    {
                        IJ.log( "The segmentation was interrupted!" );
                        IJ.showStatus( "The segmentation was interrupted!" );
                        IJ.showProgress( 1.0 );
                        return;
                    }

                    // Apply watershed
                    IJ.log("Running watershed...");

                    ImageStack resultStack = null;

                    try{
                        resultStack = Watershed.computeWatershed( imposedMinima, labeledMinima,
                                connectivity, calculateDams );
                    }
                    catch( Exception ex )
                    {
                        ex.printStackTrace();
                        IJ.log( "Error while runing watershed: " + ex.getMessage() );
                    }
                    catch( OutOfMemoryError err )
                    {
                        err.printStackTrace();
                        IJ.log( "Error: the plugin run out of memory. Please use a smaller input image." );
                    }
                    if( null == resultStack )
                    {
                        IJ.log( "The segmentation was interrupted!" );
                        IJ.showStatus( "The segmentation was interrupted!" );
                        IJ.showProgress( 1.0 );
                        return;
                    }

                    ImagePlus resultImage = new ImagePlus( "watershed", resultStack );
                    resultImage.setCalibration( image.getCalibration() );


                    // Adjust min and max values to display
                    Images3D.optimizeDisplayRange( resultImage );

                    byte[][] colorMap = ColorMaps.CommonLabelMaps.fromLabel( ColorMaps.CommonLabelMaps.GOLDEN_ANGLE.getLabel() ).computeLut( 255, false );
                    ColorModel cm = ColorMaps.createColorModel(colorMap, Color.RED);
                    resultImage.getProcessor().setColorModel(cm);
                    resultImage.getImageStack().setColorModel(cm);
                    resultImage.updateImage();

                    int slice = image.getCurrentSlice();

                    ImageRoi roi = null;
                    roi = new ImageRoi(0, 0, resultImage.getImageStack().getProcessor( slice ) );
                    roi.setOpacity( 1.0/3.0 );


                    image.setOverlay( new Overlay( roi ) );

                    createContourImage(image, resultImage);

                    createSegmentImage(image, resultImage);

                    createROIImage(image, resultImage, gradientStack);
                    // set thread to null
                    segmentationThread = null;
                }
            };

            segmentationThread = newThread;
            newThread.start();

        try {
            newThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    private void createROIImage(ImagePlus originalImage, ImagePlus resultImage, ImageStack gradientStack) {

        int slice = originalImage.getCurrentSlice();
        originalImage.setStack( gradientStack );
        originalImage.setSlice( slice );
        originalImage.updateImage();


        ImagePlus roiImage = getWatershedLines( resultImage );
        roiImage = ColorImages.binaryOverlay( originalImage, roiImage, Color.red ) ;

        IJ.save(roiImage, this.contourBaseFolder+"src\\assets\\ROI\\marcacion_ROI.jpg");
    }

    private void createContourImage(ImagePlus originalImage, ImagePlus resultImage) {
        ImagePlus contourImage = getWatershedLines( resultImage );
        contourImage = ColorImages.binaryOverlay( originalImage, contourImage, Color.red ) ;

        IJ.save(contourImage, this.contourBaseFolder+"src\\assets\\Contour\\marcacion_contour.jpg");
    }

    private void createSegmentImage(ImagePlus originalImage, ImagePlus resultImage) {

        int slice = originalImage.getCurrentSlice();
        originalImage.setStack( originalImage.getImageStack() );
        originalImage.setSlice( slice );
        originalImage.updateImage();

        ImageStack inputStackCopy = originalImage.getImageStack().duplicate();
        ImagePlus displayImage = new ImagePlus( originalImage.getTitle(),
                inputStackCopy );

        displayImage.setStack( inputStackCopy );
        displayImage.setTitle("Morphological Segmentation");
        displayImage.setSlice( originalImage.getCurrentSlice() );

        // set the 2D flag
        boolean inputIs2D = originalImage.getImageStackSize() == 1;

        // correct Fiji error when the slices are read as frames
        if ( inputIs2D == false &&
                displayImage.isHyperStack() == false &&
                displayImage.getNSlices() == 1 )
        {
            // correct stack by setting number of frames as slices
            displayImage.setDimensions( 1, displayImage.getNFrames(), 1 );
        }


        ImagePlus segmentImage = displayImage.duplicate();
        segmentImage.setOverlay( null ); // remove existing overlay
        ImageStack is = new ImageStack( displayImage.getWidth(), displayImage.getHeight() );

        for( slice=1; slice<=segmentImage.getImageStackSize(); slice++ )
        {
            ImagePlus aux = new ImagePlus( "", segmentImage.getImageStack().getProcessor( slice ) );
            ImageRoi roi = new ImageRoi(0, 0, resultImage.getImageStack().getProcessor( slice ) );
            roi.setOpacity( 1.0/3.0 );
            aux.setOverlay( new Overlay( roi ) );
            aux = aux.flatten();
            is.addSlice( aux.getProcessor() );
        }

        segmentImage.setStack( is );

        segmentImage.getImageStack().addSlice(originalImage.getImageStack().getProcessor(originalImage.getCurrentSlice()));

        IJ.save(segmentImage, this.contourBaseFolder+"src\\assets\\Segment\\marcacion_segment.jpg");
    }

    private ImagePlus getWatershedLines( ImagePlus labels )
    {
        ImagePlus lines = BinaryImages.binarize( labels );
        lines.getImageStack().getProcessor(lines.getCurrentSlice()).invert();

        return lines;
    }

}
