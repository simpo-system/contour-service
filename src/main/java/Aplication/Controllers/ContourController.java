package Aplication.Controllers;

import Aplication.Services.ContourService;
import Models.ContourRequest;
import Models.ContourResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
public class ContourController {

    private ContourService contourService;

    public ContourController(ContourService contourService) {
        this.contourService = contourService;
    }

    @RequestMapping(path="/contour", method=POST)
    public ContourResponse contour(@RequestBody ContourRequest request){
        return this.contourService.contour(request.getTcImages());
    }
}
