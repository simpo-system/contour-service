package Models;

import java.util.List;

public class ContourResponseData {
    private final int id;
    private final ContourResponseImages images;

    public ContourResponseData(int id, ContourResponseImages images) {
        this.id = id;
        this.images = images;
    }

    public int getId() {
        return id;
    }

    public ContourResponseImages getImages() {
        return images;
    }
}
