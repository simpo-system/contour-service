package Models;

import java.util.List;

public class ContourRequest {

    private List<ContourRequestData> tcImages;

    public ContourRequest(){

    }

    public ContourRequest (List<ContourRequestData> tcImages) {
        this.tcImages = tcImages;
    }

    public List<ContourRequestData> getTcImages() {
        return tcImages;
    }

}
