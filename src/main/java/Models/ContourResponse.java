package Models;

import java.util.List;

public class ContourResponse {

    private final List<ContourResponseData> data;

    public ContourResponse(List<ContourResponseData> data) {
        this.data = data;
    }

    public List<ContourResponseData> getData() {
        return data;
    }
}
