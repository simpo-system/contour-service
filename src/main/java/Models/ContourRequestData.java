package Models;

public class ContourRequestData {
    private  int id;
    private  String imageUrl;
    private String imageName;

    public ContourRequestData() {
    }

    public ContourRequestData(int id, String imageUrl, String imageName) {
        this.id = id;
        this.imageUrl = imageUrl;
        this.imageName = imageName;

    }

    public int getId() {
        return id;
    }

    public String getImageUrl() {
        return imageUrl;
    }


    public String getImageName() {
        return imageName.substring(0, imageName.indexOf("."));
    }
}
