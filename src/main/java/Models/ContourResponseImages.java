package Models;

public class ContourResponseImages {

    private final String contourImageUrl;
    private final String roiImageUrl;
    private final String segmentImageUrl;

    public ContourResponseImages (String contourImageUrl, String roiImageUrl, String segmentImageUrl) {
        this.contourImageUrl = contourImageUrl;
        this. roiImageUrl = roiImageUrl;
        this.segmentImageUrl = segmentImageUrl;
    }

    public String getContourImageUrl() {
        return contourImageUrl;
    }

    public String getRoiImageUrl() {
        return roiImageUrl;
    }

    public String getSegmentImageUrl() {
        return segmentImageUrl;
    }


}
